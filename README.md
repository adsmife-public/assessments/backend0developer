# Apigate Coding Assessment (Backend)

### Introduction 
This assessment is designed to explore your backend development skills. You are tasked with creating a simple API adaptor that translates API responses and provides an authentication layer.

### The scenario
A telco operator has agreed to expose their customer information to Apigate over a RESTful API. The API can be accessed at: https://394d03xt7c.execute-api.ap-southeast-1.amazonaws.com/staging/user-info/{msisdn}. 

At Apigate, we have a standard API specification for exposing customer information - the specification document can be found in this repo. Your task is to write a simple adaptor that will translate the operator's response to Apigate's standard response and expose Apigate's standard response via a RESTful API. 

In order to prevent misuse of Apigate's customer info API, you will need to implement an authentication mechanism. The current standard that we adhere to is RFC6749, Authorization Code Grant (Bearer + Refresh). 

### Additional notes
The fields in both APIs will not necessarily overlap. Please use your best judgement in dealing with missing fields. Should you find RFC6749 overly complicated to implement, feel free to use a different API authenticatio mechanism and let us know your selection (ie Basic auth?)

For the telco operator's API call, replace {msisdn} with a relevant phone number, ie - 60122770647

### Submission guide
Please fork this repo and drop your contact person a note once you are done

### Technology
Feel free to use any modern technology and open source framework. 

### How we review
- **Corretcness**: Does the adaptor function as expected?
- **Code quality**: Is the code easy to understand and maintain? Is the coding style consistent with the language's best practices?
- **Performance**: What is the maximium throughput/TPS of the adaptor?

### Bonus points
- **Documentation** - Is the README well written? Are the commit messages clear? Is there a Swagger spec?
- **Automated Tests** - Are there any automated tests?
- **Production-readiness** - Is there proper error handling? Is the code ready to to put into production?
- **Logging** - Are there clear and useful logs?
- **Fancy Features** - Response caching? Response throttling? HATEOAS? Surprise us :)
